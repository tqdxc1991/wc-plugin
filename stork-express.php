<?php
/*
Plugin Name: Stork Express
Plugin URI: https://woocommerce.com/
Description: Adds Carrier options operated by Stork Express. Configuration of working times and preparation time available in the plugin.
Version: 1.0.0
Author: Stork SAS
Author URI: https://woocommerce.com/
*/

/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	function stork_express_init() {
		if ( ! class_exists( 'WC_Stork_Express' ) ) {
			class WC_Stork_Express extends WC_Shipping_Method {
				/**
				 * Constructor for your shipping class
				 *
				 * @access public
				 * @return void
				 */
				public function __construct() {
					$this->id                 = 'stork_express'; // Id for your shipping method. Should be uunique.
					$this->method_title       = __( 'Stork Express Shipping' );  // Title shown in admin
					$this->method_description = __( 'Description of your shipping method' ); // Description shown in admin
					$this->supports           = array(
						'shipping-zones',
						'instance-settings',
					);
					$this->enabled            = "yes"; // This can be added as an setting but for this example its forced enabled
					$this->title              = "Stork Express Shipping"; // This can be added as an setting but for this example its forced.

					$this->init();
				}

                /**
                 * Initialise Gateway Settings Form Fields
                 */
                public function init_form_fields() {
                    $this->form_fields = array(
                    'title' => array(
                        'title' => __( 'Stork Express Shipping' ),
                        'type' => 'text',
                        'description' => __( 'This controls the title which the user sees during checkout.'),
                        'default' => __( 'Stork Express')
                        ),
                    'description' => array(
                        'title' => __( 'Description'),
                        'type' => 'textarea',
                        'description' => __( 'This controls the description which the user sees during checkout.'),
                        'default' => __("Here is my new shipping plugin!I can configure it using the following configuration form.This plugin will boost your sales!")
						),
						'live_mode' => array(
							'title' => 'Live mode',
							'description' => 'Stork Express live mode',
							'type' => 'select',
							'default' => '1',
							'options' => array(
								 'yes' => '1',
								 'no' => '0'
								) // array of options for select/multiselects only
							),
							'password' => array(
								'title' => __( 'Password'),
								'type' => 'password',
								'description' => __( 'Your account password'),
								'default' => null
								),

							);
                } // End init_form_fields()

				public function admin_options() {
					?>
					<h2><?php _e('Stork Express'); ?></h2>
					<table class="form-table">
					<?php $this->generate_settings_html(); ?>
					</table> <?php
					}

				/**
				 * Init your settings
				 *
				 * @access public
				 * @return void
				 */
				public function init() {
					// Load the settings API
					$this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
					$this->init_settings(); // This is part of the settings API. Loads settings you previously init.

					// Save settings in admin if you have any defined
					add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
				}

				/**
				 * calculate_shipping function.
				 *
				 * @access public
				 * @param mixed $package
				 * @return void
				 */
				public function calculate_shipping( $package ) {
					$rate = array(
						'label' => $this->title,
						'cost' => '10.99',
						'calc_tax' => 'per_item'
					);

					// Register the rate
					$this->add_rate( $rate );
				}
			}
		}
	}

	add_action( 'woocommerce_shipping_init', 'stork_express_init' );

	function add_stork_express( $methods ) {
		$methods['stork_express'] = 'WC_Stork_Express';
		return $methods;
	}

	add_filter( 'woocommerce_shipping_methods', 'add_stork_express' );
}